# CHANGELOG

## [0.1.2] - 2023-05-28

### Fixed

- Fixed tests not correctly marked as passed
- Fixed whitespace issues in the program's output

### Added

- Tests commands are now timed out after 30s
- Add a `timeout` key to the tests metadata to specify a custom timeout

## [0.1.1] - 2023-05-28

### Added

- Defined the syntax of a clins test file
- Added Recursive parsing of all clins files in the current directory
- Added the execution of each clins test file

## [0.1.0] - 2023-05-17

_The first release of cl-inspect_
