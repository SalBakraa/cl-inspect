# CL-Inspect

## Description

CL-Inspect is a testing framework for command line utilities. CL-Inspect uses a custom syntax for it's test files.
The test file syntax is detailed below.

## Test File Syntax

```
--- Each test starts with a test metadata section. The section is delimited by 2 lines started by `---`
name: Each test *MUST* have a name specified
cmd: Each test *MUST* have a command to execute. The command is executed by through `sh -c` on linux and `cmd \\C` on windows
exit_status: Each test *MIGHT* have an exit code. The test fails if the command exits with a different code. Default is 0.
timeout: Each test *MIGHT* timeout. The test fails if the command runs longer than the timeout. Default is 30s.
---

Explanation_of_the_IO_block_syntax <<<

	After the test metadata comes the I/O section. Each I/O segment is started by `FILENAME IO_DELIMITER` and is
	subsequently terminated by `IO_DELIMITER FILENAME`. The input delimiter is `<<<` while the output delimiter is
	`>>>`.

	The contents each I/O block are split based on either `LF` or `CRLF` which inturn means that the newline characters
	are not preserved.

	The closing delimiter's filename must exactly match the filename specified in the opening delimiter or else it will
	be treated as if it's part or the input's content.

<<< Different filename

<<< Explanation_of_the_IO_block_syntax

Showcasing_the_ability_to_extend_the_clins_syntax <<< rust
fn main() {
	println!("This syntax was inspired by both the shell's heredoc and markdown code blocks");
	println!("Any text add after the starting delimiter is ignored");
}
<<< Showcasing_the_ability_to_extend_the_clins_syntax

Explanation of Each IO Block Types >>> This is an output block

	Each I/O block specifies a file that will be created during test execution. Input blocks will created by CL-Inspect
	while output blocks will be created by the command. CL-Inspect will not pass the names of the input files to the
	test command so you should add them yourself.

	The tests will fail if command doesn't create the file specified by the output block or if the contents of the
	generated file differ from the contents specified in the output block.

	There 3 special IO blocks. `stdin`, `stdout`, and `stderr`. The contents of the `stdin` block are piped into the
	command. `stdout` and `stderr` only fail if command's output doesn't match.

>>> Explanation of Each IO Block Types

--- Each clins file file is treated as test group that contains multiple tests
name: Each test section is terminated by either another test or `EOF`
cmd: Test don't need to have and I/O blocks specified
---
```

### Test Example

```
---
name: This is passing test with no IO
cmd: true
---

---
name: This is failing test with no IO
cmd: false
---

---
name: The should pass if the command fails
cmd: false
exit_status: 1
---

---
name: This is test that timeouts
cmd: sleep 1h
timeout: 5s
---

---
name: This is typical cat test
cmd: cat input_file_1 "input file 2"
---

input_file_1 <<<
This is file 1 and it contains some text that
will be concatenated with file 2
<<< input_file_1

input file 2 <<<
This is file 2 and it has new text
that will be appended to file 1
<<< input file 2

stdout >>>
This is file 1 and it contains some text that
will be concatenated with file 2
This is file 2 and it has new text
that will be appended to file 1
>>> stdout
```
