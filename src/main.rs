use std::io::{self, BufRead, Lines, BufReader, Write, Cursor};
use std::fs::{self, DirEntry, File};
use std::fmt::Display;
use std::ops::Range;
use std::iter::{Peekable, Enumerate};
use std::path::{Path, PathBuf};
use std::sync::{Mutex, mpsc};
use std::time::Duration;
use std::thread;
use std::borrow::Cow;
use std::process::{self, Stdio};

use bstr::ByteSlice;
use itertools::{Itertools, EitherOrBoth::*};
use imara_diff::{Algorithm, diff};
use imara_diff::intern::InternedInput;
use os_str_bytes::OsStrBytes;
use wait_timeout::ChildExt;
use duration_string::DurationString;

#[macro_use]
extern crate enum_display_derive;

fn main() {
	let test_files = Mutex::new(Vec::new());
	if let Err(err) = process_files(Path::new("."), &|e| {
		let path = e.path();
		if path.as_os_str().to_raw_bytes().ends_with(b".clins") {
			test_files.lock().unwrap().push(path);
		}
	}) {
		panic!("IO error: {err}");
	};

	let test_files = test_files.into_inner().unwrap();
	let mut groups = test_files.into_iter().filter_map(|file| {
		let lines = read_lines(&file);
		if let Err(err) = lines  {
			panic!("Read error: {err}");
		}

		let lines = unsafe { lines.unwrap_unchecked() };
		let tests = parse_test_cases(lines);
		if let Err((line_idx, line, err)) = tests {
			eprintln!("{}:{line_idx}: error: {err}", file.display());
			if let Some(line) = line {
				eprintln!("\t {line}\n");
			}
			return None;
		}

		let tests = unsafe { tests.unwrap_unchecked() };
		if tests.is_empty() {
			eprintln!("{}: warning: no test cases have been defined\n", file.display());
			return None;
		}

		return Some(TestGroup { file, tests });
	}).collect::<Vec<_>>();

	if groups.is_empty() {
		eprintln!("No test files found in current directory.");
		return;
	}

	println!("Test Files: ");
	for g in &groups {
		println!("{}", g.file.display());
	}
	println!();

	for g in &mut groups {
		run_test_group(g);
	}
	println!();

	let mut there_was_failure = false;

	// Print faliure/error details
	groups.iter().for_each(|g| {
		g.tests.iter().filter(|t| {
			t.status == TestStatus::Error || t.status == TestStatus::Failed
		}).for_each(|t| {
			there_was_failure = true;

			println!(">>>> {}/{} <<<<", unsafe { g.file.file_stem().unwrap_unchecked() }.to_raw_bytes().as_bstr(), t.name);
			println!("Status: {}", t.status);
			if let Some(msg) = &t.failure_details.reason {
				println!("\t- {msg}");
				if let Some(details) = &t.failure_details.details {
					print!("{details}");
				}
			}
			println!();
		});
	});

	// Summaries the Failed tests
	if there_was_failure {
		println!("Failures:");
		groups.iter().for_each(|g| {
			g.tests.iter().filter(|t| {
				t.status == TestStatus::Error || t.status == TestStatus::Failed
			}).for_each(|t| {
				println!("\t{}/{} ... {}", unsafe { g.file.file_stem().unwrap_unchecked() }.to_raw_bytes().as_bstr(), t.name, t.status);
			});
			println!();
		});
	}

	// Summaries all test groups
	let mut total_ran = 0;
	let mut total_passed = 0;
	let mut total_failed = 0;
	let mut total_error = 0;
	groups.iter().for_each(|g| {
		g.tests.iter().for_each(|t| {
			total_ran += 1;
			match t.status {
				TestStatus::NotRun => total_ran -= 1,
				TestStatus::Failed => total_failed += 1,
				TestStatus::Passed => total_passed += 1,
				TestStatus::Error => total_error += 1,
			}
		});
	});
	println!("Test Summary: ");
	println!("\tGroups: {} | Tests ran: {total_ran} | Passed: {total_passed} | Failed: {total_failed} | Errors: {total_error}", groups.len());

}

// one possible implementation of walking a directory only visiting files
fn process_files(dir: &Path, cb: &impl Fn(&DirEntry)) -> io::Result<()> {
	if !dir.is_dir() {
		return Ok(())
	}

	for entry in fs::read_dir(dir)? {
		let entry = entry?;
		let path = entry.path();
		if path.is_dir() {
			process_files(&path, cb)?;
		} else {
			cb(&entry);
		}
	}

	Ok(())
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<Lines<BufReader<File>>>
where P: AsRef<Path>, {
	let file = File::open(filename)?;
	Ok(BufReader::new(file).lines())
}

struct TestGroup {
	file: PathBuf,
	tests: Vec<TestData>,
}

#[derive(Default)]
struct TestData {
	name: String,
	cmd: String,
	exit_status: i32,
	timeout: Duration,

	inputs: Vec<Stream>,
	outputs: Vec<Stream>,
	stdin: Option<Vec<String>>,
	stdout: Option<Vec<String>>,
	stderr: Option<Vec<String>>,

	status: TestStatus,
	failure_details: TestFailureMessage,
}

#[derive(Default)]
struct Stream {
	path: PathBuf,
	content: Vec<String>,
}

#[derive(Default, Display, PartialEq, Eq)]
enum TestStatus {
	#[default] NotRun, Failed, Passed, Error
}

#[derive(Default)]
struct TestFailureMessage {
	reason: Option<Cow<'static, str>>,
	details: Option<Cow<'static, str>>,
}

#[derive(Debug)]
enum ParserState {
	Initial,
	TestCaseMetadata,
	TestCaseIO,
}

fn parse_test_cases(lines: Lines<BufReader<File>>) -> Result<Vec<TestData>, (usize, Option<String>, Cow<'static, str>)> {
	let lines = lines.enumerate();
	let mut test_cases: Vec<TestData> = Vec::new();
	let mut peek_lines = lines.peekable();
	while let Some(line_data) = peek_lines.peek() {
		let (line_idx, line) = line_data;
		if let Err(err) = line {
			panic!("Read error: {err}");
		}

		let idx = *line_idx;
		let test_case = parse_test_case(&mut peek_lines)?;
		if test_case.name.is_empty()  {
			// Due to either an empty test group or a trailing empty lines at the end of the test file
			continue;
		}

		for t in &test_cases {
			if t.name == test_case.name {
				return Err((idx, None, format!("error: test case `{}` has already been defined.", test_case.name).into()));
			}
		}
		test_cases.push(test_case);
	}
	return Ok(test_cases);
}

fn parse_test_case(lines: &mut Peekable<Enumerate<Lines<BufReader<File>>>>) -> Result<TestData, (usize, Option<String>, Cow<'static, str>)> {
	let mut state = ParserState::Initial;
	let mut test_case = TestData::default();
	test_case.timeout = Duration::from_secs(30); // Set default timeout to 30s

	let mut test_case_start = None;
	while let Some(line_data) = lines.peek() {
		let (line_idx, line) = line_data;
		if let Err(err) = line {
			panic!("Read error: {err}");
		}

		let line = line.as_ref().unwrap();
		let line = line.trim();
		match state {
			ParserState::Initial => {
				if line.is_empty() {
					lines.next();
					continue;
				}

				if !line.starts_with("---") {
					return Err((*line_idx, Some(line.to_string()), "expected `---` at start of test case".into()));
				}

				state = ParserState::TestCaseMetadata;
				test_case_start = Some(*line_idx);
				lines.next();
			},

			ParserState::TestCaseMetadata => {
				if line.is_empty() {
					lines.next();
					continue;
				}

				if line.starts_with("---") {
					state = ParserState::TestCaseIO;
					lines.next();
					continue;
				}

				if let Some(name) = line.strip_prefix("name:") {
					test_case.name = name.trim_start().to_owned();
					lines.next();
					continue;
				}

				if let Some(cmd) = line.strip_prefix("cmd:") {
					test_case.cmd = cmd.trim_start().to_owned();
					lines.next();
					continue;
				}

				if let Some(exit) = line.strip_prefix("exit_status:") {
					match exit.trim_start().parse() {
						Ok(code) => test_case.exit_status = code,
						Err(err) => return Err((*line_idx, Some(line.to_string()), format!("failed to parse exit code: {err}").into())),
					}
					lines.next();
					continue;
				}

				if let Some(time) = line.strip_prefix("timeout:") {
					match time.trim_start().parse::<DurationString>() {
						Ok(duration) => test_case.timeout = duration.into(),
						Err(err) => return Err((*line_idx, Some(line.to_string()), format!("failed to parse timeout duration: {err}").into())),
					}
					lines.next();
					continue;
				}

				return Err((*line_idx, Some(line.to_string()), "unknown key-value pair".into()));
			},

			ParserState::TestCaseIO => {
				if line.is_empty() {
					lines.next();
					continue;
				}

				if line.starts_with("---") {
					break;
				}

				let mut is_instream = true;
				let mut delim_start = line.find("<<<");
				if let None = delim_start {
					is_instream = false;
					delim_start = line.find(">>>");
				}

				if let None = delim_start {
					return Err((*line_idx, Some(line.to_string()), "expected IO specifier start".into()));
				}

				// Own the current line se we can pass references to it if we need to parse
				let line = line.to_owned();
				let idx = *line_idx;

				let delim_start = unsafe { delim_start.unwrap_unchecked() };
				let path = &line[..delim_start].trim();
				let trailing_data = &line[delim_start+3..].trim();

				if path.contains("/") {
					return Err((idx, Some(line.to_string()), "multi-component paths are not supported".into()));
				}

				// Skip the current line
				lines.next();
				let content = if trailing_data != path {
					let delim = &line[delim_start..delim_start+3];
					let content = parse_io_content(lines, path, delim);
					if let Err(..) = content {
						return Err((idx, Some(line.to_string()), format!("missing IO terminator `{delim} {path}").into()));
					}

					unsafe { content.unwrap_unchecked() }
				} else {
					return Err((idx, Some(line.to_string()), "single line IO streams are not supported".into()));
				};

				match *path {
					"stdin" => test_case.stdin = if test_case.stdin == None {
						Some(content)
					} else {
						return Err((idx, Some(line.to_string()), "stdin stream has already been defined".into()));
					},

					"stdout" => test_case.stdout = if test_case.stdout == None {
						Some(content)
					} else {
						return Err((idx, Some(line.to_string()), "stdout stream has already been defined".into()));
					},

					"stderr" => test_case.stderr = if test_case.stderr == None {
						Some(content)
					} else {
						return Err((idx, Some(line.to_string()), "stderr stream has already been defined".into()));
					},

					_ => {
						let stream = if is_instream { &mut test_case.inputs } else { &mut test_case.outputs };
						stream.push(
							Stream {
								path: path.into(),
								content,
							}
						);
					},
				}
			},
		}
	}

	// Empty test cases are valid
	if let None = test_case_start {
		return Ok(test_case);
	}

	let test_case_start = unsafe { test_case_start.unwrap_unchecked() };
	if test_case.name.is_empty() {
		return Err((test_case_start, None, "error: missing test case name".into()));
	}

	if test_case.cmd.is_empty() {
		return Err((test_case_start, None, format!("error: command missing for test case `{}`", test_case.name).into()));
	}

	return Ok(test_case);
}

fn parse_io_content(lines: &mut Peekable<Enumerate<Lines<BufReader<File>>>>, path: &str, delim: &str) -> Result<Vec<String>, ()> {
	let mut content = Vec::new();
	while let Some(line_data) = lines.peek() {
		let (_, line) = line_data;
		if let Err(err) = line {
			panic!("Read error: {}", err);
		}

		let line = unsafe { line.as_ref().unwrap_unchecked() };
		let line = line.trim();
		if line.starts_with(delim) && (&line[delim.len()..]).trim() == path {
			content.push(String::new());
			lines.next();
			return Ok(content);
		}
		content.push(line.to_owned());
		lines.next();
	}
	return Err(());
}

const LINE_SEP: &'static str = if cfg!(target_os = "windows") { "\r\n" } else { "\n" };

fn run_test_group(group: &mut TestGroup) {
	let tmpdir = tempfile::Builder::new().prefix(unsafe { &group.file.file_stem().unwrap_unchecked() }).tempdir();
	if let Err(err) = tmpdir {
		panic!("\tFailed to create temp directory for tests: {err}");
	}

	// Let the width of the first diff column be a quarter of the terminal
	let c_width: usize = if let Some(s) = termsize::get() {
		s.cols >> 2
	} else {
		0
	}.into();

	println!("Running: {}", unsafe { &group.file.file_stem().unwrap_unchecked() }.to_raw_bytes().as_bstr());
	let tmpdir = unsafe { tmpdir.unwrap_unchecked() };
	'testing: for test in &mut group.tests {
		if test.status != TestStatus::NotRun {
			continue;
		}

		let tmpdir = tempfile::Builder::new().prefix(&test.name).tempdir_in(&tmpdir);
		if let Err(err) = tmpdir {
			panic!("\tFailed to create temp directory for tests: {err}");
		}

		print!("\t{} ... ", test.name);
		if let Err(err) = io::stdout().flush() {
			panic!("Couldn't flush stdout: {err}");
		}

		let (tx, rx) = mpsc::channel();
		let timer_thread = thread::spawn(move || {
			let mut duration = Duration::default();
			let millie = Duration::from_millis(1);
			let sleep_dur = Duration::from_micros(900);

			let mut durstr = format!("{duration:?}");
			print!("({durstr})");
			if let Err(err) = io::stdout().flush() {
				panic!("Couldn't flush stdout: {err}");
			}

			loop {
				print!("\x1b[{}D\x1b[K", durstr.len()+2);
				if let Ok(str) = rx.try_recv() {
					println!("{str} {duration:?}");
					break;
				}

				durstr = format!("{duration:?}");
				print!("({duration:?})");
				if let Err(err) = io::stdout().flush() {
					panic!("Couldn't flush stdout: {err}");
				}

				thread::sleep(sleep_dur);
				duration += millie;
			}
		});

		let close_thread = |thread: thread::JoinHandle<()>, msg: &'static str| {
			tx.send(msg).unwrap();
			thread.join().unwrap();
		};

		let tmpdir = unsafe { tmpdir.unwrap_unchecked() };
		for stream in &test.inputs {
			if let Err(err) = create_file_from_stream(stream, tmpdir.path()) {
				test.status = TestStatus::Error;
				test.failure_details.reason = Some(format!("Failed to create test's input file `{}`", stream.path.display()).into());
				test.failure_details.details = Some(format!("Error: `{err}`\n").into());
				close_thread(timer_thread, "error after:");
				continue 'testing;
			}
		}

		let mut command = if cfg!(target_os = "windows") {
			let mut command = process::Command::new("cmd");
			command.arg("\\C");
			command
		} else {
			let mut command = process::Command::new("sh");
			command.arg("-c");
			command
		};

		command.current_dir(&tmpdir).arg(&test.cmd).stdout(Stdio::piped()).stderr(Stdio::piped());
		if test.stdin.is_some() {
			command.stdin(Stdio::piped());
		}

		let child = command.spawn();
		if let Err(err) = child {
			test.status = TestStatus::Error;
			test.failure_details.reason = Some("Error spawning child process running test command".into());
			test.failure_details.details = Some(format!("Error: `{err}`\n").into());
			close_thread(timer_thread, "error after:");
			continue 'testing;
		}

		let mut child = unsafe { child.unwrap_unchecked() };
		if let Some(test_in) = &test.stdin {
			let c_stdin = child.stdin.take();
			if let None = c_stdin {
				test.status = TestStatus::Error;
				test.failure_details.reason = Some(format!("Could not retrieve child process' stdin").into());
				close_thread(timer_thread, "error after:");
				continue 'testing;
			}
			if let Err(err) = unsafe { c_stdin.unwrap_unchecked() }.write_all(test_in.join(LINE_SEP).as_bytes()) {
				test.status = TestStatus::Error;
				test.failure_details.reason = Some("Error writing to child process' stdin".into());
				test.failure_details.details = Some(format!("Error: `{err}`\n").into());
				close_thread(timer_thread, "error after:");
				continue 'testing;
			}
		}

		if let None = child.wait_timeout(test.timeout).unwrap() {
			child.kill().unwrap();
			test.status = TestStatus::Error;
			test.failure_details.reason = Some(format!("Command timed out after {:?}", test.timeout).into());
			close_thread(timer_thread, "error after:");
			continue 'testing;
		}

		let out = child.wait_with_output();
		if let Err(err) = out {
			test.status = TestStatus::Error;
			test.failure_details.reason = Some("Failed to wait for child process' output".into());
			test.failure_details.details = Some(format!("Error: `{err}`\n").into());
			close_thread(timer_thread, "error after:");
			continue 'testing;
		}

		let out = unsafe { out.unwrap_unchecked() };
		match out.status.code() {
			None => panic!("Test process was terminated through a signal"),
			Some(code) => {
				if code != test.exit_status {
					test.status = TestStatus::Failed;
					test.failure_details.reason = Some("Command exited with unexpected exit code: {code}".into());
					close_thread(timer_thread, "failure after:");
					continue 'testing;
				}
			},
		}

		// I assume that a `Cursor` can never fail due to it not doing IO operation
		if let Some(t_stdout) = &test.stdout {
			let mut prog_stdout: Vec<_> = Cursor::new(&out.stdout).lines()
				.map(|l| unsafe { l.unwrap_unchecked() }).collect();
			prog_stdout.push(String::new()); // Add back the deleted terminating newline character

			// Get the longest line in the program
			let diff_prefix = format!("{:c_width$}      {}", "expected stdout", "actual stdout");
			let diff_result = DiffResult::from_strings(t_stdout.join(LINE_SEP), prog_stdout.join(LINE_SEP), c_width, diff_prefix);
			if diff_result.has_diff {
				test.status = TestStatus::Failed;
				test.failure_details.reason = Some("Command stdout output doesn't match expected content".into());
				test.failure_details.details = Some(diff_result.diff_str.into());
				close_thread(timer_thread, "failure after:");
				continue 'testing;
			}
		}

		if let Some(t_stderr) = &test.stderr {
			let mut prog_stderr: Vec<_> = Cursor::new(&out.stderr).lines()
				.map(|l| unsafe { l.unwrap_unchecked() }).collect();
			prog_stderr.push(String::new()); // Add back the deleted terminating newline character

			let diff_prefix = format!("{:c_width$}      {}", "expected stderr", "actual stderr");
			let diff_result = DiffResult::from_strings(t_stderr.join(LINE_SEP), prog_stderr.join(LINE_SEP), c_width, diff_prefix);
			if diff_result.has_diff {
				test.status = TestStatus::Failed;
				test.failure_details.reason = Some("Command stderr output doesn't match expected content".into());
				test.failure_details.details = Some(diff_result.diff_str.into());
				close_thread(timer_thread, "failure after:");
				continue 'testing;
			}
		}

		for stream in &test.outputs {
			let file_path = tmpdir.path().join(&stream.path);
			if !file_path.exists() {
				test.status = TestStatus::Failed;
				test.failure_details.reason = Some("Command failed generating expected file".into());
				close_thread(timer_thread, "failure after:");
				continue 'testing;
			}

			let mut lines: Vec<_> = read_lines(file_path).expect("Read Error")
				.map(|l| l.unwrap()).collect();
			lines.push(String::new()); // Add back the deleted terminating newline character

			let diff_prefix = format!("expected {:c_width$}      actual {0}", stream.path.display());
			let diff_result = DiffResult::from_strings(stream.content.join(LINE_SEP), lines.join(LINE_SEP), c_width, diff_prefix);
			if diff_result.has_diff {
				test.status = TestStatus::Failed;
				test.failure_details.reason = Some("Generated file content doesn't match expected content".into());
				test.failure_details.details = Some(diff_result.diff_str.into());
				close_thread(timer_thread, "faliure after:");
				continue 'testing;
			}
		}

		test.status = TestStatus::Passed;
		close_thread(timer_thread, "success after:");
	}
}

#[inline]
fn create_file_from_stream(stream: &Stream, dir: &Path) -> Result<(), io::Error> {
	let mut file = File::create(dir.join(&stream.path))?;
	write!(file, "{}", stream.content.join(LINE_SEP))
}

#[derive(Default)]
struct DiffResult {
	has_diff: bool, // Whether this was any differences found or not
	diff_str: String, //  The diff string generated
}

impl DiffResult {
	fn from_strings(before: String, after: String, c_width: usize, prefix: impl AsRef<str>) -> DiffResult {
		let input = InternedInput::new(before.as_str(), after.as_str());

		let mut pos  = 0;
		let mut buff = Vec::with_capacity(1024);
		writeln!(buff, "{}", prefix.as_ref()).unwrap();

		let mut has_diff = false;
		let sink = |before: Range<u32>, after: Range<u32>| {
			// This is called only if there is difference
			has_diff = true;

			let hunk_before: Vec<_> = input.before[before.start as usize..before.end as usize]
				.iter()
				.map(|&line| &input.interner[line])
				.collect();
			let hunk_after: Vec<_> = input.after[after.start as usize..after.end as usize]
				.iter()
				.map(|&line| &input.interner[line])
				.collect();

			if hunk_after.is_empty() {
				Self::print_common_lines(&input, &mut buff, c_width, &mut pos, before.start, before.end + 1);
				for line in hunk_before {
					let mut was_truncated = false;
					let line = truncate(line, c_width, &mut was_truncated);
					let fill_str = if was_truncated { "" } else { "   " };
					writeln!(buff, "\x1b[0;31m{line:<c_width$}{:.<3}    <", fill_str, c_width = c_width - 3).unwrap();
				}
			} else if hunk_before.is_empty() {
				Self::print_common_lines(&input, &mut buff, c_width, &mut pos, before.start, before.start);
				for line in hunk_after {
					writeln!(buff, "\x1b[0;32m{:<c_width$}    > {line}", "").unwrap();
				}
			} else {
				Self::print_common_lines(&input, &mut buff, c_width, &mut pos, before.start, before.end);
				for pair in hunk_before.iter().zip_longest(&hunk_after) {
					match pair {
						Both(bef, af) => {
							let mut was_truncated = false;
							let bef = truncate(bef, c_width - 3, &mut was_truncated);
							let fill_str = if was_truncated { "" } else { "   " };
							writeln!(buff, "\x1b[0;33m{bef:<c_width$}{:.<3}    | {af}", fill_str, c_width = c_width - 3)
						},
						Left(bef)     => {
							let mut was_truncated = false;
							let bef = truncate(bef, c_width - 3, &mut was_truncated);
							let fill_str = if was_truncated { "" } else { "   " };
							writeln!(buff, "\x1b[0;31m{bef:<c_width$}{:.<3}    <", fill_str, c_width = c_width - 3)
						},
						Right(af)     => writeln!(buff, "\x1b[0;32m{:<c_width$}    > {af}", ""),
					}.unwrap();
				}
			}
			write!(buff, "\x1b[0m").unwrap();
		};

		diff(Algorithm::Histogram, &input, sink);
		DiffResult { has_diff, diff_str: unsafe { String::from_utf8_unchecked(buff) } }
	}

	#[inline(always)]
	fn print_common_lines(input: &InternedInput<&str>, buff: &mut Vec<u8>, c_width: usize, start_pos: &mut u32, end_pos: u32, new_pos: u32) {
		input.before[*start_pos as usize..end_pos as usize]
			.iter()
			.for_each(|&l| {
				let mut was_truncated = false;
				let line = truncate(&input.interner[l], c_width - 3, &mut was_truncated);
				let fill_str = if was_truncated { "" } else { "   " };
				writeln!(buff, "{:<c_width$}{:.<3}      {0}", line, fill_str, c_width = c_width - 3).unwrap()
			});
		*start_pos = (new_pos).min(input.before.len() as u32);
	}
}

#[inline(always)]
fn truncate<'a>(s: &'a (impl AsRef<str> + 'a), max_chars: usize, was_truncated: &mut bool) -> &'a str {
	match s.as_ref().char_indices().nth(max_chars) {
		None => { *was_truncated = false; s.as_ref() },
		Some((idx, _)) => { *was_truncated = true; &s.as_ref()[..idx] },
	}
}
